
using Microsoft.AspNetCore.Mvc;

namespace openapifilter.Controllers
{
	[ApiController]
	public class HomeController : Controller
	{
		[HttpGet]
		[Route("")]
		public string Index()
		{
			return "Hallo, weldt!";
		}

		[HttpPost]
		[Route("UserFoo")]
		public string UserFoo(string userName)
		{
			return "ha";
		}

		[HttpPost]
		[Route("UserBar")]
		public string UserBar(string userName)
		{
			return "na";
		}
	}
}