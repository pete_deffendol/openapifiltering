# OpenApi Filtering example
## The important little bit
```c#
			app.UseSwagger(x => x.PreSerializeFilters.Add(
				(doc,httpRequest) => {
					if (!httpRequest.Headers.ContainsKey("aspnet-auth")) //really whatever else we want
					{
						var routes = doc.Paths.Where(x=>x.Key.Contains("Foo"));
						foreach (var element in routes)
						{
							Console.WriteLine($"Removing {element.Key}");
							doc.Paths.Remove(element.Key);
						}
					}
				}
			));
```