﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;

namespace openapifilter
{
    public class SwaggerAuthorizedEndpointFilter
    {
        public SwaggerAuthorizedEndpointFilter(/* stuff we need here */)
        {

        }

        public void Filter(OpenApiDocument doc, HttpRequest httpRequest)
        {
            if (!httpRequest.Headers.ContainsKey("aspnet-auth"))
            {
                var routes = doc.Paths.Where(x => x.Key.Contains("Foo"));
                foreach (var element in routes)
                {
                    Console.WriteLine($"Removing {element.Key}");
                    doc.Paths.Remove(element.Key);
                }
            }
        }
    }
}
